module bitbucket.org/uwaploe/insreplay

go 1.13

require (
	bitbucket.org/uwaploe/go-dvl v0.9.6
	bitbucket.org/uwaploe/go-ins v1.5.0
	bitbucket.org/uwaploe/go-must v0.7.0
	github.com/nats-io/stan.go v0.6.0
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)
