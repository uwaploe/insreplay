// Insreplay reads a MUST INS archive file and replays the data records
// through a NATS Streaming Server.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/go-ins"
	"bitbucket.org/uwaploe/go-must"
	"github.com/nats-io/stan.go"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: insreplay [options] file

Read MUST INS records from file and publish them to a NATS Streaming Server
at the same rate they were generated.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	realTime = flag.Bool("realtime", false,
		"Time shift the data records to now")
	natsURL    string = "nats://localhost:4222"
	clusterID  string = "must-cluster"
	insSubject string = "ins.data"
	subSample  int    = 5
	qLen       int    = 4
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&insSubject, "sub", lookupEnvOrString("INS_SUBJECT", insSubject),
		"Subject name for INS data")
	flag.IntVar(&subSample, "n", lookupEnvOrInt("INS_SUBSAMPLE", subSample),
		"Subsample factor for the input data")
	flag.IntVar(&qLen, "qlen", lookupEnvOrInt("INS_QLEN", qLen),
		"Size of data record queue")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {

	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(0)
	}

	fin, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}

	sc, err := stan.Connect(clusterID, "ins-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	rdr := must.NewInsUdReader(fin)
	// Collect the time intervals between records
	dt := rdr.Intervals()
	dt = append(dt, 0)
	// Rewind the file
	fin.Seek(0, 0)

	// Start a worker to publish the data
	queue := make(chan ins.DataRecord, qLen)
	defer close(queue)

	go func() {
		for rec := range queue {
			msg, err := msgpack.Marshal(&rec)
			if err != nil {
				log.Printf("Msgpack encoding failed: %v", err)
				continue
			}
			sc.Publish(insSubject, msg)
		}
	}()

	log.Printf("INS replay starting (%s)", Version)

	// Make a second pass through the data file and publish
	i := int(0)
	t := time.Now()
	for rdr.Scan() {
		rec := rdr.Record().(ins.DataRecord)
		if *realTime {
			ns := t.UTC().UnixNano()
			rec.Tsec = ns / 1e9
			rec.Tnsec = int32(ns % 1e9)
		}

		if (i % subSample) == 0 {
			select {
			case queue <- rec:
			default:
				log.Println("WARNING: queue overflow")
			}
		}
		t = <-time.After(dt[i])
		i++
	}

	log.Println("Replay done")
}
